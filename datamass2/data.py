# -*- coding: utf-8 -*-
from simple_salesforce import Salesforce
import sys, json

with open('../boxconfig/boxconfig.json', 'r') as f:
    boxconfig = json.load(f)

if len(sys.argv) == 1:
    print "Erro: Parametro nao informado: Nome da box"
    sys.exit()
try:
    box = boxconfig[sys.argv[1]]
    box['nome'] = sys.argv[1]
    print 'Box Selecionada: %s' % box['nome']
except KeyError:
      print 'Nome da box nao encontrado'
      sys.exit(2)

# Carrega configurações
with open('config.json', 'r') as f:
    config = json.load(f)

# Inicializa o client do Salesforce e realiza o login
sf = Salesforce(username=box['username'], password=box['password'], security_token=box['security_token'], domain=box['domain'])


# ###################################################################################
# 11 - Habilitar a estrutura de produção com o Name "60.5808.1967.1990055.1990055" 
#   para a conta Auwen IMOBILIARIA HELDT LTDA, trocando a corretora para Auwen.
# ###################################################################################
print "Habilitar a estrutura de producao para a conta Auwen"
sql = "Select id, name From Account Where name like 'AUWEN%%'"
rs = sf.query( sql )

if rs['totalSize'] == 0:
    print "conta auwen nao encontrado"
    sys.exit()
else:
    contaAuwen = rs['records'][0]

sql = "SELECT Id, COD_EV1__r.DETALHE_DO_CORRETOR__r.ATUACAO__r.id,  COD_EV1__r.DETALHE_DO_CORRETOR__r.id"
sql += " FROM ESTRUTURA_DE_PRODUCAO__c"
sql += " WHERE TXT_ESTRUTURA_PRODUCAO_CHAVE_UNICA__c = '%s' and FLG_ATIVO__C = true" % config['EstruturaAuwen']
rs = sf.query( sql )

estrutura = rs['records'][0]
idAtuacao = estrutura['COD_EV1__r']['DETALHE_DO_CORRETOR__r']['ATUACAO__r']['Id']
idDetalhe = estrutura['COD_EV1__r']['DETALHE_DO_CORRETOR__r']['Id']

sf.ATUACAO__c.update( idAtuacao, {
    'CONTA__c': contaAuwen['Id']
})
sf.DETALHE_DO_CORRETOR__c.update( idDetalhe, {
    'CONTA__c': contaAuwen['Id']
})
sf.ESTRUTURA_DE_PRODUCAO__c.update( estrutura['Id'], {
    'TXT_ESTRUTURA_PRODUCAO_CHAVE_UNICA__c': config['EstruturaAuwen']
})
print "OK"


# ###################################################################################
# 12 - Habilitar a estrutura de produção 
# com o Name "94.10089.56336.41173.41173" para a Imob Heldt
# ###################################################################################
print "Habilitar a estrutura de producao para a conta Imob Heldt"
sql = "Select id, name From Account Where name like 'IMOBILIARIA HELDT%%'"
rs = sf.query( sql )

if rs['totalSize'] == 0:
    print "conta heldt nao encontrada"
    sys.exit()
else:
    contaHeldt = rs['records'][0]

sql = "SELECT Id, COD_EV1__r.DETALHE_DO_CORRETOR__r.ATUACAO__r.id,  COD_EV1__r.DETALHE_DO_CORRETOR__r.id"
sql += " FROM ESTRUTURA_DE_PRODUCAO__c"
sql += " WHERE Name = '%s' and FLG_ATIVO__C = true" % config['EstruturaHeldt']
rs = sf.query( sql )

estrutura = rs['records'][0]
idAtuacao = estrutura['COD_EV1__r']['DETALHE_DO_CORRETOR__r']['ATUACAO__r']['Id']
idDetalhe = estrutura['COD_EV1__r']['DETALHE_DO_CORRETOR__r']['Id']

sf.ATUACAO__c.update( idAtuacao, {
    'CONTA__c': contaHeldt['Id']
})
sf.DETALHE_DO_CORRETOR__c.update( idDetalhe, {
    'CONTA__c': contaHeldt['Id']
})
sf.ESTRUTURA_DE_PRODUCAO__c.update( estrutura['Id'], {
    'Name': config['EstruturaHeldt']
})
print "OK"