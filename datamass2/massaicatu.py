# -*- coding: utf-8 -*-
import sys, json

# Carrega configurações
with open('config.json', 'r') as f:
    config = json.load(f)

totalLinhasPorProduto = 12000

f= open("guru99.txt","w+")

for produto in config['faixas-COL']:    
    for i in range(totalLinhasPorProduto):
        f.write("INSERT INTO icatu_faixa_proposta (cod_sistema, cod_produto, num_proposta, num_seq_cobranca)")
        f.write(" VALUES ('COL', '%s', '%s', '%s');\n" % (produto,
                                                            config['faixas-COL'][produto][0] + i,
                                                            config['faixas-COL'][produto][1] + i))
for produto in config['faixas-INP']:    
    for i in range(totalLinhasPorProduto):
        f.write("INSERT INTO icatu_faixa_proposta (cod_sistema, cod_produto, num_proposta, num_seq_cobranca)")
        f.write(" VALUES ('INP', '%s', '%s', '%s');\n" % (produto,
                                                            config['faixas-INP'][produto][0] + i,
                                                            config['faixas-INP'][produto][1] + i))

f.close()