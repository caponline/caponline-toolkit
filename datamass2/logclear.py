# -*- coding: utf-8 -*-
from simple_salesforce import Salesforce
import sys, json

with open('../boxconfig/boxconfig.json', 'r') as f:
    boxconfig = json.load(f)

if len(sys.argv) == 1:
    print "Erro: Parametro nao informado: Nome da box"
    sys.exit()
try:
    box = boxconfig[sys.argv[1]]
    box['nome'] = sys.argv[1]
    print 'Box Selecionada: %s' % box['nome']
except KeyError:
      print 'Nome da box nao encontrado'
      sys.exit(2)

# Carrega configurações
with open('config.json', 'r') as f:
    config = json.load(f)

# Inicializa o client do Salesforce e realiza o login
sf = Salesforce(username=box['username'], password=box['password'], security_token=box['security_token'], domain=box['domain'])

# #################################################################
# LIMPA REGISTROS DE DEPURAÇÃO
# #################################################################
print 'LIMPA REGISTROS DE DEPURAÇÃO'
sql = "SELECT Id FROM ApexLog"
rs = sf.query( sql )

for log in rs['records']:
    print 'apagando registro: %s' % log['Id']
    sf.ApexLog.delete( log['Id'] )

print "OK"
