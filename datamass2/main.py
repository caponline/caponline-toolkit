# -*- coding: utf-8 -*-
from simple_salesforce import Salesforce
import sys, json

with open('../boxconfig/boxconfig.json', 'r') as f:
    boxconfig = json.load(f)

if len(sys.argv) == 1:
    print "Erro: Parametro nao informado: Nome da box"
    sys.exit()
try:
    box = boxconfig[sys.argv[1]]
    box['nome'] = sys.argv[1]
    print 'Box Selecionada: %s' % box['nome']
except KeyError:
      print 'Nome da box nao encontrado'
      sys.exit(2)

# Carrega configurações
with open('config.json', 'r') as f:
    config = json.load(f)

# Inicializa o client do Salesforce e realiza o login
sf = Salesforce(username=box['username'], password=box['password'], security_token=box['security_token'], domain=box['domain'])


# #################################################################
# 1 - Atualiza usuário com o nome da box
# #################################################################
print '1 - Atualiza usuario com o nome da box'
sql = "SELECT Id, FirstName, LastName, UserRoleId FROM User"
sql = sql + " Where name LIKE '%s%%' AND email = '%s'" %(config['nomeAdmin'], config['emailAdmin'])
rs = sf.query( sql )

if rs['totalSize'] == 0:
    print "usuario %s nao encontrado" % config['nomeAdmin']
    sys.exit()
else:
    user = rs['records'][0]

    if box['nome'] in user['LastName']:
        print "Usuario ja tem o nome da box: %s" % box['nome']
    else:
        nomeUser = '%s %s' % (user['LastName'], box['nome'])
        sf.User.update( user['Id'], {
            'LastName': nomeUser
        })
        print 'usuario atualizado: %s %s' % (user['FirstName'], nomeUser)
print 'OK'


# #################################################################
# 2 - Verifica se o papel do usuário está "ADM"
# #################################################################
print '2 - Verifica se o papel do usuario esta "ADM"'
sql = "SELECT Id FROM UserRole WHERE name = 'ADM'"
rs = sf.query( sql )

if rs['totalSize'] == 0:
    print "UserRole ADM nao encontrado"
    sys.exit()
else:
    userRoleADM = rs['records'][0]
    if user['UserRoleId'] == userRoleADM['Id']:
        print 'Usuario marcado como ADM'
    else:
        print 'Erro: Usuario nao ADM'
        sys.exit()
print 'OK'


# #################################################################
# 7 - Atualizar URL da faixa de propostas Icatu
# #################################################################
print '7 - Atualizar faixa de proposta'
sql = "SELECT Id FROM CS_CAP_CONFIG__c WHERE Name = '%s'" % config['servicoIcatu']['nome']
rs = sf.query( sql )

if rs['totalSize'] == 0:
    print 'Chave de configuracao %s nao encontrada, inserir' % config['servicoIcatu']['nome']
    print config['servicoIcatu']['endpoint']
    sf.bulk.CS_CAP_CONFIG__c.insert([{
        'Name': config['servicoIcatu']['nome'],
        'SERVICE_HTTP_METHOD__c': config['servicoIcatu']['metodo'],
        'SERVICE_URL__c': config['servicoIcatu']['endpoint'],
        'SERVICE_LOGIN__c': config['servicoIcatu']['login'],
        'SERVICE_PASSWORD__c': config['servicoIcatu']['token'],
        'SERVICE_ENABLED__c': True
    }])

else:
    print 'Atualizando URL da geracao do boleto'
    print config['servicoIcatu']['endpoint']
    itemConfigBoleto = rs['records'][0]
    sf.CS_CAP_CONFIG__c.update( itemConfigBoleto['Id'], {
        'SERVICE_HTTP_METHOD__c': config['servicoIcatu']['metodo'],
        'SERVICE_URL__c': config['servicoIcatu']['endpoint'],
        'SERVICE_LOGIN__c': config['servicoIcatu']['login'],
        'SERVICE_PASSWORD__c': config['servicoIcatu']['token'],
        'SERVICE_ENABLED__c': True
    })

print 'OK'


# #################################################################
# 7.1 - Atualizar URL da geração de boleto
# #################################################################
print '7.1 - Atualizar URL da geracao de boleto'
sql = "SELECT Id FROM CS_CAP_CONFIG__c WHERE Name = '%s'" % config['servicoBoleto']['nome']
rs = sf.query( sql )

if rs['totalSize'] == 0:
    print 'Erro: Chave de configuracao %s nao encontrada' % config['servicoBoleto']['nome']
    sys.exit()
else:
    print 'Atualizando URL da geracao do boleto'
    print config['servicoBoleto']['endpoint']
    itemConfigBoleto = rs['records'][0]
    sf.CS_CAP_CONFIG__c.update( itemConfigBoleto['Id'], {
        'SERVICE_HTTP_METHOD__c': config['servicoBoleto']['metodo'],
        'SERVICE_URL__c': config['servicoBoleto']['endpoint'],
        'SERVICE_LOGIN__c': config['servicoBoleto']['login'],
        'SERVICE_PASSWORD__c': config['servicoBoleto']['token'],
        'SERVICE_ENABLED__c': True
    })
print 'OK'


# ###################################################################################
# 10 - Cadastrar imagens de Banner incluindo o nome do ambiente no objeto Banner
# ###################################################################################
print '10 - Cadastrar imagens de Banner incluindo o nome do ambiente no objeto Banner'
sql = "SELECT Id FROM BANNER__c"
rs = sf.query( sql )

if rs['totalSize'] == 0:
    print "Banner nao encontrado"
    sf.bulk.BANNER__c.insert([
        {
            'Name' : 'Banner CapOnline %s' % box['nome'],
            'DAT_FIM__c' : '2030-12-12',
            'DAT_INICIO__c' : '2019-10-10',
            'DSC_LINK__c' : 'http://www.disney.com',
            'FLG_ATIVO__c' : True,
            'NUM_ORDEM__c' : 1,
            'FLG_DISPONIVEL_APENAS_INTERNAMENTE__c' : False
        }
    ])
    print "Banner inserido"
else:
    print "Banner ja encontrado"
print 'OK'