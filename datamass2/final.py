# -*- coding: utf-8 -*-
from simple_salesforce import Salesforce
import sys, json

with open('../boxconfig/boxconfig.json', 'r') as f:
    boxconfig = json.load(f)

if len(sys.argv) == 1:
    print "Erro: Parametro nao informado: Nome da box"
    sys.exit()
try:
    box = boxconfig[sys.argv[1]]
    box['nome'] = sys.argv[1]
    print 'Box Selecionada: %s' % box['nome']
except KeyError:
      print 'Nome da box nao encontrado'
      sys.exit(2)

# Carrega configurações
with open('config.json', 'r') as f:
    config = json.load(f)

# Inicializa o client do Salesforce e realiza o login
sf = Salesforce(username=box['username'], password=box['password'], security_token=box['security_token'], domain=box['domain'])

# #################################################################
# 15 - Setar no objeto SUBTIPO__c o campo FLG_ATIVO__c = true
# #################################################################
print '15 - Atualizando SubTipo'
sql = "select Id from SUBTIPO__c"
rs = sf.query( sql )

for subtipo in rs['records']:
    sf.SUBTIPO__c.update( subtipo['Id'], {
        'FLG_ATIVO__c': True
    })
print "OK"

# #################################################################
# 16 - Setar algumas propostas como o owner o usuário AUWEN
# #################################################################
print '16 - Setar algumas propostas como o owner o usuario AUWEN'
sql = "Select Id, name From User Where name like '%%auwen%%'"
rs = sf.query( sql )

if rs['totalSize'] == 0:
    print "usuario auwen nao encontrado"
    sys.exit()
else:
    userAuwen = rs['records'][0]


sql = "SELECT Id, FirstName, LastName, UserRoleId FROM User Where name LIKE '%s%%'" % config['nomeAdmin']
rs = sf.query( sql )

if rs['totalSize'] == 0:
    print "usuario %s nao encontrado" % config['nomeAdmin']
    sys.exit()
else:
    userAdmin = rs['records'][0]


sql = "select Id from PROPOSTA__c where OwnerId = '%s'" % userAdmin['Id']
rs = sf.query( sql )

for proposta in rs['records']:
    print 'Atualizando proposta %s' % proposta['Id']
    sf.PROPOSTA__c.update( proposta['Id'], {
        'OwnerId': userAuwen['Id']
    })
print "OK"
    

# #################################################################
# 17 - Setar algumas comissões para o usuario da Auwen
# #################################################################
print '17 - Setar algumas comissoes para o usuario da Auwen'
sql = "Select COD_ESTRUTURA_DE_VENDA__c From Account Where name like 'Auwen%'"
rs = sf.query( sql )

if rs['totalSize'] == 0:
    print "conta auwen nao encontrada"
    sys.exit()
else:
    contaAuwen = rs['records'][0]


sql = "select Id from COMISSAO_PAGA__c"
rs = sf.query( sql )

if rs['totalSize'] == 0:
    print 'Comissao paga nao encontrada'
    sys.exit()
else:
    for comissao in rs['records']:
        sf.COMISSAO_PAGA__c.update( comissao['Id'], {
            'COD_ESTRUTURA_VENDA__c': contaAuwen['COD_ESTRUTURA_DE_VENDA__c'],
            'TXT_CPF_CNPJ_ADM__c': None
        })
print "OK"


# #################################################################
# 18 - Setar algumas remuneracoes para o usuario da Heldt
# #################################################################
print '18 - Setar algumas remuneracoes para o usuario da Heldt'
sql = "select Id from COMISSAO_PAGA__c limit 10"
rs = sf.query( sql )

for comissao in rs['records']:
    sf.COMISSAO_PAGA__c.update( comissao['Id'], {
        'TXT_CPF_CNPJ_ADM__c': config['cnpjHeldt']
    })
print "OK"


# #################################################################
# 19 - Entitlement (Direito):
# #################################################################
print '19 - Entitlement (Direito):'
print '  a) add SAS Account'
sql = "select id, Name, CNPJ__c, recordTypeId from account where Name = 'SUL AMERICA CAPITALIZACAO S.A.' limit 1"
rs = sf.query( sql )

if rs['totalSize'] == 0:
    sf.bulk.Account.insert([
        {
            'Name': 'SUL AMERICA CAPITALIZACAO S.A.',
            'CNPJ__c': '33.040.924/0001-70',
            'RecordTypeId': '01231000001IcUqAAK'
        }
    ])
    print "Account Sulamerica inserido"
else:
    print "Account Sulamerica ja cadastrado"

print "  b) Add the Entitlement"
sql = "select Id from Entitlement Where name = 'CAPOnline - Aprovação de Manifestação'"
rs = sf.query( sql )

if rs['totalSize'] == 0:
    print 'Entitlement nao encontrado - inserindo'

    sql = "select id, Name, CNPJ__c, recordTypeId from account where Name = 'SUL AMERICA CAPITALIZACAO S.A.' limit 1"
    rs = sf.query( sql )
    contaSulamerica = rs['records'][0]

    sf.bulk.Entitlement.insert([
        {
            'Name': 'CAPOnline - Aprovação de Manifestação',
            'AccountId': contaSulamerica['Id'],
            'Type': 'Suporte telefônico',
            'StartDate': '2018-05-23',
            'SlaProcessId': '5520Z000000FSEgQAO',
            'BusinessHoursId': '01m0Z0000004DrIQAU',
            'IsPerIncident': False
        }
    ])
    print 'Entitlement inserido para Sulamerica'
else:
    print 'Entitlement ja existente OK'

print "  c) Check if it was created"
sql = "select Id, Name, AccountId, Type, StartDate, SlaProcessId, BusinessHoursId, IsPerIncident, Status FROM Entitlement WHERE Name = 'CAPOnline - Aprovação de Manifestação'"
rs = sf.query( sql )

if rs['totalSize'] == 0:
    print 'Erro: Entitlement NAO criado'
else:
    print 'Entitlement criado com SUCESSO'
print "OK"