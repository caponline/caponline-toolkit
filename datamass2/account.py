# -*- coding: utf-8 -*-
from simple_salesforce import Salesforce
import sys, json

with open('../boxconfig/boxconfig.json', 'r') as f:
    boxconfig = json.load(f)

if len(sys.argv) == 1:
    print "Erro: Parametro nao informado: Nome da box"
    sys.exit()
try:
    box = boxconfig[sys.argv[1]]
    box['nome'] = sys.argv[1]
    print 'Box Selecionada: %s' % box['nome']
except KeyError:
      print 'Nome da box nao encontrado'
      sys.exit(2)

# Carrega configurações
with open('config.json', 'r') as f:
    config = json.load(f)

# Inicializa o client do Salesforce e realiza o login
sf = Salesforce(username=box['username'], password=box['password'], security_token=box['security_token'], domain=box['domain'])


# #################################################################
# 1 - Ajusta os campos de account que sao obrigatorios na proposta
# #################################################################
print 'Ajusta os campos de account que sao obrigatorios na proposta'

sql = "SELECT Id, CPF__pc, Name, SEXO__pc, PersonBirthdate, RG__pc, ORGAO_EXPEDIDOR__pc, IDC_UF__pc, DATA_DE_EXPEDICAO__pc,"
sql = sql + " PersonMobilePhone, Phone, PersonEmail, CEP_da_Correspondencia__c, Rua_de_Correspondencia__c,"
sql = sql + " Numero_do_Endereco_de_Correspondencia__c, Bairro_de_Correspondencia__c, Cidade_de_Correspondencia__c,"
sql = sql + " IDC_UF__c, CNPJ__c, FLG_ISENTO__c, INSCRICAO_ESTADUAL__c, Nome_do_contato__c, EMAIL_PESSOA_JURIDICA__c, CELULAR__c"
sql = sql + " FROM ACCOUNT"
#sql = sql + " Where IsPersonAccount = False"
rs = sf.query( sql )

if rs['totalSize'] == 0:
    print "Nenhuma conta encontrada"
    sys.exit()
else:
    for item in rs['records']:
        if item['DATA_DE_EXPEDICAO__pc'] is None:
            item['DATA_DE_EXPEDICAO__pc'] = "1998-03-23"
        if item['PersonMobilePhone'] is None:
            item['PersonMobilePhone'] = "(89) 93521-1421"
        if item['PersonEmail'] is None:
            item['PersonEmail'] = "account.mass@icatu.com.br"

        sf.Account.update( item['Id'], {
            #'DATA_DE_EXPEDICAO__pc': item['DATA_DE_EXPEDICAO__pc'],
            'PersonMobilePhone': item['PersonMobilePhone'],
            'PersonEmail': item['PersonEmail']
        })


'''
if item['CPF__pc'] is None:
    item['CPF__pc'] = "100.994.957.83"
if item['Name'] is None:
    item['Name'] = "Leo Account DataMass"
if item['SEXO__pc'] is None:
    item['SEXO__pc'] = "Masculino"
if item['PersonBirthdate'] is None:
    item['PersonBirthdate'] = "1974-09-02"
if item['RG__pc'] is None:
    item['RG__pc'] = "667.73.01"
if item['ORGAO_EXPEDIDOR__pc'] is None:
    item['ORGAO_EXPEDIDOR__pc'] = "SSP"
if item['IDC_UF__pc'] is None:
    item['IDC_UF__pc'] = "PI"
if item['Phone'] is None:
    item['Phone'] = "(89) 3521-1421"
if item['CEP_da_Correspondencia__c'] is None:
    item['CEP_da_Correspondencia__c'] = "64045-001"
if item['Rua_de_Correspondencia__c'] is None:
    item['Rua_de_Correspondencia__c'] = "R FRANCISCO F COSTA AP 201"
if item['Numero_do_Endereco_de_Correspondencia__c'] is None:
    item['Numero_do_Endereco_de_Correspondencia__c'] = "5435"
if item['Bairro_de_Correspondencia__c'] is None:
    item['Bairro_de_Correspondencia__c'] = "COPACABANA"
if item['Cidade_de_Correspondencia__c'] is None:
    item['Cidade_de_Correspondencia__c'] = "RIO DE JANEIRO"
if item['IDC_UF__c'] is None:
    item['IDC_UF__c'] = "RJ"
if item['CNPJ__c'] is None:
    item['CNPJ__c'] = "23.323.874/0001-85"
if not item['FLG_ISENTO__c'] and item['INSCRICAO_ESTADUAL__c'] is None:
    item['INSCRICAO_ESTADUAL__c'] = "543534-65"
if item['Nome_do_contato__c'] is None:
    item['Nome_do_contato__c'] = "MIMICA JUNIOR"
if item['EMAIL_PESSOA_JURIDICA__c'] is None:
    item['EMAIL_PESSOA_JURIDICA__c'] = "fefe.nunes2@golive.com.br"
if item['CELULAR__c'] is None:
    item['CELULAR__c'] = "(18)3406-9040"



'CPF__pc': item['CPF__pc'],
'Name': item['Name'],
'SEXO__pc': item['SEXO__pc'],
'PersonBirthdate': item['PersonBirthdate'],
'RG__pc': item['RG__pc'],
'IDC_UF__pc': item['IDC_UF__pc'],
'ORGAO_EXPEDIDOR__pc': item['ORGAO_EXPEDIDOR__pc'],
'Phone': item['Phone'],
'CEP_da_Correspondencia__c': item['CEP_da_Correspondencia__c'],
'Rua_de_Correspondencia__c': item['Rua_de_Correspondencia__c'],
'Numero_do_Endereco_de_Correspondencia__c': item['Numero_do_Endereco_de_Correspondencia__c'],
'Bairro_de_Correspondencia__c': item['Bairro_de_Correspondencia__c'],
'Cidade_de_Correspondencia__c': item['Cidade_de_Correspondencia__c'],
'IDC_UF__c': item['IDC_UF__c'],
'CNPJ__c': item['CNPJ__c'],
'FLG_ISENTO__c': item['FLG_ISENTO__c'],
'INSCRICAO_ESTADUAL__c': item['INSCRICAO_ESTADUAL__c'],
'Nome_do_contato__c': item['Nome_do_contato__c'],
'EMAIL_PESSOA_JURIDICA__c': item['EMAIL_PESSOA_JURIDICA__c'],
'CELULAR__c': item['CELULAR__c']
'''