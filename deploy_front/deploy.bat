@echo off

C:
REM   CONFIGURAR O CAMINHO ABAIXO PARA O SEU AMBIENTE, A UNIDADE DEVE COLOCAR NA LINHA ABAIXO
SET CAMINHO_PORTAL=\$LEO\workGIT\capitalizacao\resource-bundle\caponline-community\apps\cap-portal
SET CAMINHO_CI=\$LEO\workGIT\main-org

IF NOT EXIST %CAMINHO_PORTAL% (
    echo ================================================================================================
    echo Por favor configure o caminho da variavel CAMINHO_PORTAL dentro do script.
    echo ================================================================================================
    pause
    exit
)
cd %CAMINHO_PORTAL%

if [%1] NEQ [] (
    SET AMBIENTE_ORIG=%1
    SET AMBIENTE_DEST=%2
    SET VERSAO_JS=%3
    goto :inicio
)

echo ================================================================================================
echo  Gerador e emissor de pacotes JS para os ambientes UAT / HML (v2.0)
echo ================================================================================================
SET/P AMBIENTE_ORIG=Informe o Branch Origem (1-CAP_STAGE / 2-CAP_HML / 3-MASTER / 4-BRANCH ATUAL):
SET/P AMBIENTE_DEST=Informe o Ambiente Destino (1-CAP_STAGE / 2-CAP_HML / 3-CI / 4-BRANCH ATUAL):
SET/P VERSAO_JS=Informe a versao do pacote (1-DEV / 2-SERVER):

:inicio
echo ...
echo Iniciar deploy...
IF %AMBIENTE_ORIG%==1 	echo DE: Branch CAP_STAGE.
IF %AMBIENTE_ORIG%==2 	echo DE: Branch CAP_HML.
IF %AMBIENTE_ORIG%==3 	echo DE: Branch MASTER.
IF %AMBIENTE_ORIG%==4 	echo DE: Branch ATUAL

IF %AMBIENTE_DEST%==1 	echo PARA: Branch CAP_STAGE.
IF %AMBIENTE_DEST%==2 	echo PARA: Branch CAP_HML.
IF %AMBIENTE_DEST%==3 	echo PARA: Esteira CI/UAT Sulamerica.
IF %AMBIENTE_DEST%==4 	echo PARA: Branch ATUAL

IF %VERSAO_JS%==1 	echo VERSAO: DESENVOLVIMENTO.
IF %VERSAO_JS%==2 	echo VERSAO: PRODU?AO.

echo ...
echo ...
echo ENTRA NO BRANCH DE ORIGEM E ATUALIZA
IF %AMBIENTE_ORIG%==1 git checkout cap_stage
IF %AMBIENTE_ORIG%==2 git checkout cap_hml
IF %AMBIENTE_ORIG%==3 git checkout master
git pull

echo ...
echo ...
echo REALIZANDO O BUILD...
IF %VERSAO_JS%==1 call gulp build
IF %VERSAO_JS%==2 call gulp build:server

echo ...
echo ...
echo COPIA O ARQUIVO GERADO PARA O TEMPORARIO
copy ..\..\src\staticresources\CapOnline.resource C:\Temp
copy ..\..\src\staticresources\CapOnline.resource-meta.xml C:\Temp

del ..\..\src\staticresources\CapOnline.resource
del ..\..\src\staticresources\CapOnline.resource-meta.xml


echo ...
echo ...
echo VAI PARA O AMBIENTE DE DESTINO
IF %AMBIENTE_DEST%==1 (
    git checkout cap_stage
)
IF %AMBIENTE_DEST%==2 (
    git checkout cap_hml
)
IF %AMBIENTE_DEST%==3 (
    cd %CAMINHO_CI%
    git checkout CI
)
git pull


echo ...
echo ...
echo COPIA ARQUIVO DO TEMPORARIO PARA O LOCAL CERTO DO COMMIT
del ..\..\..\..\staticresources\CapOnline.resource
del ..\..\..\..\staticresources\CapOnline.resource-meta.xml

copy C:\Temp\CapOnline.resource ..\..\..\..\staticresources
copy C:\Temp\CapOnline.resource-meta.xml ..\..\..\..\staticresources


echo ...
echo ...
echo =========================================================================
echo CONFIRMA O COMMIT / PUSH ????????
echo =========================================================================
pause
pause
git add ../../../../staticresources/CapOnline.resource
git add ../../../../staticresources/CapOnline.resource-meta.xml
git commit -m "[CAPONLINE] Static Resource"
git push


echo ...
echo ...
echo VOLTANDO PARA O BRANCH ANTERIOR
git checkout -


echo ...
echo ...
echo FIM - TENHA UM BOM DIA ! HAVE A GOOD DAY !
pause