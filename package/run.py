# -*- coding: utf-8 -*-
import mmap

package_path = 'C:\\$LEO\\workGIT\\capitalizacao\\package.xml'

def find_in_file(file, str_find):
    retVal = False
    with open(file) as f:
        s = mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ)
        retVal = s.find(str_find) != -1
    return retVal

# Percorre os itens do arquivo assets.txt
# e vai procurando no package
f = open("data/assets.txt", "r")
for x in f:
    nomeAsset = x.replace('\n', '')
    result = find_in_file(package_path, nomeAsset)
    if not result:
        print 'ERRO - %s <---------------------------' % nomeAsset

print 'FIM'