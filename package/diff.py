# -*- coding: utf-8 -*-

# Percorre os itens do arquivo assets.txt
# e vai procurando no package
d = open("data/diff.txt", "r")
r = open("data/assets.txt", "w+")

for linha in d:
    linha = linha.replace('\n', '')

    if '-meta' in linha:
        print 'retirando meta'
    elif 'resource-bundle/caponline-community' in linha:
        print 'retirando front'
    elif '——E' in linha:
        print 'retirando exclusao'
    elif 'package.xml' in linha:
        print 'retirando package.xml'
    else:
        part = linha.split('/')
        part = part[len(part) - 1].split('.')
        linha = part[0]
        r.write(linha + '\n')

d.close()
r.close()