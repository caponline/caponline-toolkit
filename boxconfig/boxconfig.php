<?php
include('inc/database.inc.php');
include('inc/functions.inc.php');
$data = [];
$ambientes = getListAmbientes();
foreach ($ambientes as $item) {
    $data[strtolower($item['nome_box'])] = [
        "username" => $item['user_box'],
        "password" => $item['senha_box'],
        "branch_deploy" => $item['branch_deploy'],
        "security_token" => $item['token_box'],
        "domain" => 'test',
    ];
}
echo json_encode($data, JSON_PRETTY_PRINT);
?>