<?php
function getListAmbientes() {
    $sql = "SELECT nome_box, user_box, senha_box, branch_deploy, token_box";
    $sql .= " FROM ambientes_sf ORDER BY nome_box";
    $parametros = array();
    $conn = connect_database();
    $sql = prepare_query($conn, $sql, $parametros);
    $data = recordset_open($conn, $sql);
    disconnect_database($conn);
    if ($data == false) {
        return array();
    } else {
        return $data;
    }
}
?>