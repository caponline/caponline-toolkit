<?php

// ***** MySQL settings ********************************************************* //
define('DB_HOST', 'localhost');                     /** Host (HML: 10.225.213.31) */
define('DB_NAME', 'cap');                           /** The name of the database  */
define('DB_USER', 'root');                          /** MySQL database username   */
define('DB_PASSWORD', '');                          /** MySQL database password   */
define('DB_CHARSET', 'utf8');                       /** Database Charset          */
define('DB_COLLATE', '');                           /** The Database Collate type.*/


function connect_database() {

    // Tenta realizar a conexão no MYSQL
    $conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD);
    if (!$conn) { // Check
        die("Falha de conexão: " . mysqli_connect_error());
    }
   
    // Seleciona o banco de dados
    $db_selected = mysqli_select_db($conn, DB_NAME);
    if (!$db_selected) {
        die("Falha ao selecionar a base de dados: " . mysqli_error());
    }

    // Troca o charset e collation =====================
    set_charset($conn, DB_CHARSET, DB_COLLATE);

    return $conn;
}

function disconnect_database( $conn ) {
    mysqli_close($conn);
}

function set_charset( $conn, $charset = null, $collate = null ) {

    if ( function_exists( 'mysqli_set_charset' )) {
        $set_charset_succeeded = mysqli_set_charset( $conn, $charset );
    }
    if ( $set_charset_succeeded ) {
        $query = prepare_query( $conn, 'SET NAMES %s', $charset );
        if ( ! empty( $collate ) )
            $query .= prepare_query( $conn, ' COLLATE %s', $collate );
        mysqli_query( $conn, $query );
    }
}

function prepare_query( $conn, $query, $data ) {
    if ( is_null( $query ) )
        return;

    $args = func_get_args();
    array_shift( $args );
    // If args were passed as an array (as in vsprintf), move them up
    if ( isset( $args[1] ) && is_array($args[1]) ) {
        $args = $args[1];
    }                                                                                                                                                                                                                                                                                                                                                                                                                                  
    
    $query = str_replace( "'%s'", '%s', $query );   // in case someone mistakenly already singlequoted it
    $query = str_replace( '"%s"', '%s', $query );   // doublequote unquoting
    $query = preg_replace( '|(?<!%)%f|' , '%F', $query ); // Force floats to be locale unaware
    $query = preg_replace( '|(?<!%)%s|', "'%s'", $query ); // quote the strings, avoiding escaped strings like %%s

    foreach($args as $chave => $valor) {    // Faz o escapamento dos valores
        $args[$chave] = mysqli_real_escape_string( $conn, $valor );
    }

    return @vsprintf( $query, $args );
}

function recordset_open( $conn, $sql ) {

    $retorno = NULL;

    $result = mysqli_query($conn, $sql);
    $mysql_fields = mysqli_fetch_fields($result);

    $x = 0;
    foreach ($mysql_fields as &$field) {
        $campos[$x++] = $field->name;
    }

    $x = 0;
    while($row = mysqli_fetch_row($result)) {
        for($y = 0; $y < mysqli_num_fields($result); $y++) {
            $retorno[$x][$campos[$y]] = $row[$y];
        }
        $x++;
    }
    mysqli_free_result($result);

    return $retorno;
}
?>