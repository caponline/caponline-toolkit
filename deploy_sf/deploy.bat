@echo off
REM Inicializa variaveis de configuração utilizadas no processo
SET PATH_SCRIPT=%cd%
SET PATH_PROJETO=C:\$LEO\workGIT\capitalizacao
SET PATH_PACKAGE=%PATH_PROJETO%\package.xml
SET PATH_PACKER=%PATH_PROJETO%\manifest\sas-sf-packer.py
IF NOT EXIST %PATH_PROJETO% (
    echo ================================================================================================
    echo Por favor configure o caminho da variavel PATH_PROJETO dentro do script.
    echo ================================================================================================
    pause
    exit
)


if [%1] == [] (
    echo ================================================================================================
    echo Por favor informe o parametro com o nome da Box
    echo ================================================================================================
    pause
    exit
)

REM ========================================================================
REM == Monta arquivos config.bat e build.properties dinamicamente       ====
REM ========================================================================
call python config.py %1
call config.bat
del config.bat

echo ================================================================================================
echo  SalesForce Deploy command line (v2.0)
echo ================================================================================================
echo Projeto: %PATH_PROJETO%
echo Branch origem: %REPO_BRANCH%
echo Box destino: %SF_ENV_LOWER%
echo Login: %SF_LOGIN%
echo ================================================================================================

REM ==========================================
REM ===== EXECUTA SAS-PACKER =================
REM ==========================================
call exec\exec_sas_packer.bat

REM ===========================================
REM ====== EXECUTA SCRIPT ANT =================
REM ===========================================
call exec\exec_ant.bat deployFromGitAfterPacker


echo =========================================================================
echo LIMPA ARQUIVOS UTILIZADOS NO DEPLOY ????????
echo =========================================================================
pause
pause

REM ====== LIMPA PASTAS USADAS NO DEPLOY =================
cd \
cd %PATH_PROJETO%
del manifest\Deploy\ /S /Q
rmdir manifest\Deploy\ /S /Q

cd %PATH_SCRIPT%