REM ==========================================
REM ===== EXECUTA SAS-PACKER =================
REM ==========================================
cd \
cd %PATH_PROJETO%

call git checkout %REPO_BRANCH%
call git pull
call %PATH_PACKER% %PATH_PACKAGE% %PATH_PROJETO% %SF_LOGIN% %SF_PASS% %SF_TOKEN% %SF_ENV_LOWER% %SF_ENV_UPPER%

REM ==================================================================
REM ====== LIMPA CHANGES DE COMPILACAO NO SAS-PACKER =================
REM ==================================================================
git checkout manifest/simple_salesforce/__init__.pyc
git checkout manifest/simple_salesforce/api.pyc
git checkout manifest/simple_salesforce/bulk.pyc
git checkout manifest/simple_salesforce/exceptions.pyc
git checkout manifest/simple_salesforce/login.pyc
git checkout manifest/simple_salesforce/util.pyc

cd \
cd %PATH_SCRIPT%