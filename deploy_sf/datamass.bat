@echo off
REM Inicializa variaveis de configuração utilizadas no processo
SET PATH_SCRIPT=%cd%
SET PATH_PROJETO=C:\$LEO\workGIT\capsalesforcedatamass
SET PATH_PACKAGE=%PATH_PROJETO%\package.xml
SET PATH_PACKER=%PATH_PROJETO%\manifest\sas-sf-packer.py
IF NOT EXIST %PATH_PROJETO% (
    echo ================================================================================================
    echo Por favor configure o caminho da variavel PATH_PROJETO dentro do script.
    echo ================================================================================================
    pause
    exit
)


if [%1] == [] (
    echo ================================================================================================
    echo Por favor informe o parametro com o nome da Box
    echo ================================================================================================
    pause
    exit
)

REM ========================================================================
REM == Monta arquivos config.bat e build.properties dinamicamente       ====
REM ========================================================================
call python config.py %1
call config.bat
del config.bat

echo ================================================================================================
echo  SalesForce Deploy DATAMASS (v2.0)
echo ================================================================================================
echo Projeto: %PATH_PROJETO%
echo Box destino: %SF_ENV_LOWER%
echo Login: %SF_LOGIN%
echo ================================================================================================

REM ===========================================
REM ====== EXECUTA SCRIPT ANT =================
REM ===========================================
call exec\exec_ant.bat deployDataMass

pause