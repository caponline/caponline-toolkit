# -*- coding: utf-8 -*-
import sys, json

with open('../boxconfig/boxconfig.json', 'r') as f:
    boxconfig = json.load(f)

if len(sys.argv) == 1:
    print "Erro: Parametro nao informado: Nome da box"
    sys.exit()
try:
    box = boxconfig[sys.argv[1]]
    box['nome'] = sys.argv[1]
    print 'Box Selecionada: %s' % box['nome']
except KeyError:
      print 'Erro: Nome da box nao encontrado'
      sys.exit()


'''
Gera o arquivo config.bat uma forma de passar as informações do Json do boxconfig para o bat em execução
'''
f= open("config.bat","w+")
f.write("SET REPO_BRANCH=%s" % box['branch_deploy'])
f.write("\n")
f.write("SET SF_ENV_LOWER=%s" % box['nome'].lower())
f.write("\n")
f.write("SET SF_ENV_UPPER=%s" % box['nome'].upper())
f.write("\n")
f.write("SET SF_LOGIN=%s" % box['username'])
f.write("\n")
f.write("SET SF_PASS=%s" % box['password'])
f.write("\n")
f.write("SET SF_TOKEN=%s" % box['security_token'])
f.write("\n")
f.close()

'''
Gera o arquivo build.properties necessario para o processo ANT
'''
f= open("build.properties","w+")
f.write("sf.target.username=%s" % box['username'])
f.write("\n")
f.write("sf.target.password=%s%s" % (box['password'], box['security_token']))
f.write("\n")
f.write("sf.target.serverurl=https://test.salesforce.com")
f.write("\n")
f.write("sf.maxPoll=100")
f.write("\n")
f.write("sf.pollWaitMillis=40000")
f.write("\n")
f.write("pypacker.dest=C:\\\\$LEO\\\\workGIT\\\\capitalizacao\\\\manifest\\\\Deploy")
f.write("\n")
f.write("datamass.dest=C:\\\\$LEO\\\\workGIT\\\\capsalesforcedatamass")
f.write("\n")
f.close()