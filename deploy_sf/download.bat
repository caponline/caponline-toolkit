@echo off
REM Inicializa variaveis de configuração utilizadas no processo
SET PATH_SCRIPT=%cd%
SET PATH_ASSETS=%PATH_SCRIPT%\download\assets

REM Apaga a pasta de assets e recria
@RD /S /Q %PATH_ASSETS%
MD %PATH_ASSETS%

echo Nessa pasta os assets que estiverem no package serao descarregados > %PATH_ASSETS%\readme.txt

if [%1] == [] (
    echo ================================================================================================
    echo Por favor informe o parametro com o nome da Box
    echo ================================================================================================
    pause
    exit
)

REM ========================================================================
REM == Monta arquivos config.bat e build.properties dinamicamente       ====
REM ========================================================================
call python config.py %1
call config.bat
del config.bat

echo ================================================================================================
echo  SalesForce Download command line (v2.0)
echo ================================================================================================
echo Login: %SF_LOGIN%
echo ================================================================================================

REM ===========================================
REM ====== EXECUTA SCRIPT ANT =================
REM ===========================================
call exec\exec_ant.bat download